﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core_Almacen.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Core_Almacen.Controllers
{
    public class AlmacenController : Controller
    {

        private readonly ApplicationDbContext _db;

        public AlmacenController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            var displaydata = _db.Almacen.ToList();
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string empSearch)
        {
            ViewData["GetAlmacenDetails"] = empSearch;
            var empquery = from x in _db.Almacen select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Producto.Contains(empSearch) || x.Marca.Contains(empSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        public IActionResult Create()
        {
            return View();  
        }

        [HttpPost]
        public async Task<IActionResult> Create(Almacen nEmp)
            { 
            if (ModelState.IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");  

            }

            return View(nEmp);  
        }

        public async Task<ActionResult> Detail(int? id) 
        {
            if (id == null)
            {
                return RedirectToAction("Index");

            }

            var getEmpDetail = await _db.Almacen.FindAsync(id);
            return View(getEmpDetail);
        }


        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Almacen.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Almacen oldEmp)
        {
            if (ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }

            var getEmpDetail = await _db.Almacen.FindAsync(id);
            return View(getEmpDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpDetail = await _db.Almacen.FindAsync(id);
            _db.Almacen.Remove(getEmpDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
