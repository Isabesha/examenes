﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core_Almacen.Models
{
    public class Almacen
    {
        [Key]
        public int Empid { get; set; }

        [Required(ErrorMessage = "*Campo obligatorio")]
        [Display(Name = "Producto")]
        public string Producto { get; set; }

        [Required(ErrorMessage = "*Campo obligatorio")]
        [Display(Name = "Marca")]
        public string Marca { get; set; }

        [Required(ErrorMessage = "*Campo obligarotio (no más de 50)")]
        [Display(Name = "Cantidad")]
        [Range(20, 50)]
        public int Cantidad { get; set; }

        [Required(ErrorMessage = "*Campo obligatorio")]
        [Display(Name = "Fecha de llegada")]
        public string Llegada { get; set; }

        [Required(ErrorMessage = "*Campo obligatorio")]
        [Display(Name = "Fecha de caducidad")]
        public string Caducidad { get; set; }

     
    }
}
