<?php
//incluir archivo de conexion
include("conexion.php");

//crear la consulta para listar datos

$consulta="SELECT id_usuar, nombre, apellido, area, pago, f_ingreso, f_vencido FROM usuarios";

$ejecuta=$conexion -> query($consulta) or die ("Error al listar los usuarios <br> :" . $conexion -> error);

?>

<!--A partir de aquí es código html-->
<table id="lista_usuarios">   <!--nombre de la tabla que se crea para dar presentación a la vista!-->
  <tr>
    <th>ID  </th>
    <th>Nombe</th>
    <th>Apellido</th>
    <th>Área</th>
    <th>Pago</th>
    <th>Fecha de ingreso</th>
    <th>Fecha de vencimiento</th>
    <th colspan="2"> Opciones</th>
  </tr>

<?php

  while ($arreglo_resultados = $ejecuta -> fetch_row() ){
    echo '<tr>';
    echo '<td>'.$arreglo_resultados[0].'</td>';
    echo '<td>'.$arreglo_resultados[1].'</td>';
    echo '<td>'.$arreglo_resultados[2].'</td>';
    echo '<td>'.$arreglo_resultados[3].'</td>';
    echo '<td>'.$arreglo_resultados[4].'</td>';
$formato1= date("d/m/Y", strtotime($arreglo_resultados[5]));
    echo '<td>'.$formato1.'</td>';
$formato2= date("d/m/Y", strtotime($arreglo_resultados[6])); //se cambia el formato de la fecha para que sea mejor visualmente en la lista.
    echo '<td>'.$formato2.'</td>';
    echo '<td> <button class="btn" type="button" onclick="formUsuario('.$arreglo_resultados[0].');">';
    		echo 'Editar</button></td>';
    	//boton para eliminar
    		echo '<td> <button class="btn" type="button" onclick="eliminarUsuario('.$arreglo_resultados[0].');">';
    		echo 'Eliminar</button></td>';
    		echo '</tr>';
    	}
?>
</table>

<style>
.btn{margin:10px 0 10px 0;color:black;font-weight:bold;display:inline-block;padding:6px 12px;font-size:16px;text-align:center;cursor:pointer;background-image:none;border:1px solid transparent;border-radius:4px;outline: 0;}
.fb{background-color:#3b5998;}
.fb:hover{background-color:#6b93e7;}
.gp{background-color:#dd4b39;}
.gp:hover{background-color:#f87362;}


</style>
