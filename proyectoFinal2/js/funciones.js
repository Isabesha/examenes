function formUsuario(id_usuario){
  window.location="php/editarUsuario.php?id=" + id_usuario;
}

function abre_nuevo_form(){
  window.location="php/editarUsuario.php";
}


function eliminarUsuario(id_usuario){
  //enviamos request de manera asincrona
  //alert(id_producto); esto fue punicamente para realizar una prueba de que se estaba llamando correctamente la funcion
  $.ajax({
    type:'POST',
    url: 'php/eliminaUsuario.php',
    data: {id : id_usuario},
    cache : false,
    success: function (mi_respuesta){
      alert(mi_respuesta); //lanza el resultado del request
      location.reload(); // recarga la página
    }
  });
}
