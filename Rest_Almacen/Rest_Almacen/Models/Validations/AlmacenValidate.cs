﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rest_Almacen.Models
{
    [MetadataType(typeof(Almacen.MetaData))]
    public partial class Almacen
    {
        sealed class MetaData
        {
            [Key]
            public int Empid;

            [Required(ErrorMessage = "*Campo obligatorio")]
            public string Producto;

            [Required(ErrorMessage = "Campo obligatorio")]
            public string Marca;

            [Required]
            [Range(0, 50, ErrorMessage = "*Cantidad inválidad")]
            public Nullable<int> Cantidad;

            [Required(ErrorMessage = "Campo obligatorio")]
            public string Llegada;

            [Required(ErrorMessage = "Campo obligatorio")]
            public string Caducidad;
        }


        }
}